﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CheckIn_CheckOut
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Login", id = UrlParameter.Optional }
            );
             routes.MapRoute(
                name: "diem-kiem-dich",
                url: "diem-kiem-dich",
                defaults: new { controller = "DiemKiemDich", action = "Index" }
            );
             routes.MapRoute(
                "Edit",
                "thong-tin/{id}", // URL
                new { controller = "DiemKiemDich", action = "ChiTiet", name = UrlParameter.Optional }, // URL Defaults
                new { id = @"\d+" } // URL Constraints
            );
              routes.MapRoute(
                name: "diem-kiem-dich-dangnhap",
                url: "dang-nhap",
                defaults: new { controller = "DiemKiemDich", action = "Login" }
            );
             routes.MapRoute(
                name: "dang-ky-di-chuyen",
                url: "dang-ky-di-chuyen",
                defaults: new { controller = "KhaiBaoDiChuyen", action = "KhaiBao" }
            );
             routes.MapRoute(
                name: "khai-bao-y-te",
                url: "khai-bao-y-te",
                defaults: new { controller = "KhaiBaoDiChuyen", action = "KhaiBaoYTe" }
            );
                routes.MapRoute(
                 "nhap-thong-tin",
                 "nhap-thong-tin/{id}",
                 new { controller = "NhapThongTin", action = "KhaiBao", name = UrlParameter.Optional },
                 new { id = @"\d+" } // URL Constraints
              );
            routes.MapRoute(
                name: "dang-ky-thanh-cong",
                url: "dang-ky-thanh-cong",
                defaults: new { controller = "KhaiBaoDiChuyen", action = "ThanhCong" }
            );
        }
    }
}
