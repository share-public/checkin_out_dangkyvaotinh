using CheckIn_CheckOut.Helper_Code.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace CheckIn_CheckOut
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);



            // API authorization registration.    
            GlobalConfiguration.Configuration.MessageHandlers.Add(new AuthorizationHeaderHandler());
            //GlobalConfiguration.Configuration.Formatters.XmlFormatter.UseXmlSerializer = true;
        }
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null || authCookie.Value == "")
                return;
            FormsAuthenticationTicket authTicket;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch
            {
                return;
            }
            if (authTicket.Expiration < DateTime.Now)
            {
                FormsAuthentication.SignOut();
                return;
            }
            // retrieve roles from UserData
            string[] roles = authTicket.UserData.Split(';');

            //https://stackoverflow.com/questions/1385042/asp-net-mvc-forms-authentication-authorize-attribute-simple-roles
            // retrieve roles from UserData
            if (authTicket.UserData == null)
                return;

            //get username from ticket
            string username = authTicket.Name;

            Context.User = new GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(username, "MyCustomAuthTypeName"), authTicket.UserData.Split(';'));

            if (Context.User != null)
                Context.User = new GenericPrincipal(Context.User.Identity, roles);
        }
        protected void Application_EndRequest(Object sender,
                                           EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (context.Response.Status.Substring(0, 3).Equals("401"))
            {
                context.Response.Redirect("~/Home/Login");
            }
            if (context.Response.Status.Substring(0, 3).Equals("404"))
            {
                context.Response.Redirect("~/Home/Login");
            }
        }
    }
}
