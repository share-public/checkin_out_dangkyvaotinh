﻿using CheckIn_CheckOut.Models;
using PagedList;
using QRCoder;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CheckIn_CheckOut.Controllers
{
    public class HomeController : Controller
    {
        String taikhoansms = ConfigurationManager.AppSettings["TaiKhoanSMS"];
        String matkhausms = ConfigurationManager.AppSettings["MatKhauSMS"];
        String brand = ConfigurationManager.AppSettings["BrandName"];
        String API = ConfigurationManager.AppSettings["APIlink"];

        DataConnectDataContext data = new DataConnectDataContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //[Authorize(Roles = "DanhSach")]
        //public ActionResult TrangChu()
        //{
        //    return View();
        //}
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadDSDonVi(string tpid)
        {
            if (string.IsNullOrEmpty(tpid))
                return Json(HttpNotFound());
            var categoryList = GetDonViList(Convert.ToInt32(tpid));
            var categoryData = categoryList.Select(m => new SelectListItem()
            {
                Text = m.TenXaPhuong,
                Value = m.IdXaPhuong.ToString()
            });
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }
        private IList<XaPhuong> GetDonViList(int tpid)
        {
            return data.XaPhuongs.OrderBy(c => c.TenXaPhuong).Where(c => c.IDQuanHuyen == tpid).ToList();
        }
        // load danh sach tỉnh thành
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadDSDonViHuyen(string tpid)
        {
            if (string.IsNullOrEmpty(tpid))
                return Json(HttpNotFound());
            var categoryList = GetDonViListQuan(Convert.ToInt32(tpid));
            var categoryData = categoryList.Select(m => new SelectListItem()
            {
                Text = m.TenQuanHuyen,
                Value = m.IDQuanHuyen.ToString()
            });
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }
        private IList<QuanHuyen> GetDonViListQuan(int tpid)
        {
            return data.QuanHuyens.OrderBy(c => c.TenQuanHuyen).Where(c => c.IDTinhThanh == tpid).ToList();
        }
        [Authorize(Roles = "DanhSach")]
        public ActionResult TaoDiemCheck()
        {
            ViewData["LoaiDoiTuong"] = new SelectList(data.LoaiDoiTuongs.ToList().OrderBy(n => n.IDLoaiDoiTuong), "IDLoaiDoiTuong", "TenLoai");
            ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");
            return View();
        }
        [Authorize(Roles = "DanhSach")]
        [HttpPost]
        public ActionResult TaoDiemCheck(FormCollection collection)
        {
            ViewData["LoaiDoiTuong"] = new SelectList(data.LoaiDoiTuongs.ToList().OrderBy(n => n.IDLoaiDoiTuong), "IDLoaiDoiTuong", "TenLoai");
            ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");
            var sodienthoai = Session["SoDienThoai"].ToString();
            var tendiadiem = collection["TenDiaDiem"];

            var kiemtraten = data.DiemKiemDiches.Where(n => n.SoDienThoai == sodienthoai && n.TenDiemKiemDich == tendiadiem);
            if(kiemtraten.Count()>0)
            {
                ViewBag.Thongbao = "Tồn tại tên địa điểm!";
                return View();
            }
            var idloai = int.Parse(collection["LoaiDoiTuong"]);
            var idxa = int.Parse(collection["ListDonVi"]);
            var diachi = collection["DiaChi"];
            var list= data.Ins_DiemKiemDich(tendiadiem, sodienthoai, idloai, idxa, diachi);
            Session["IDDiemKiemDich"] = list.ToArray()[0].IdDiemKiemDich;
            Session["TenDiemKiemDich"] = tendiadiem;
            return RedirectToAction("QRCode_DiaDiem", "Home");
        }
        [Authorize(Roles = "DanhSach")]
        public ActionResult DanhSachDiemKiemDich()
        {
            try
            {

            var ds = data.Get_DanhSachDiemKiemDich_SDT(Session["SoDienThoai"].ToString());
            return View(ds);
            }catch(Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }
         }
        [Authorize(Roles = "DanhSach")]
        public ActionResult Delete(int id)
        {
            var ds = data.Xoa_sodienthoai(id, Session["SoDienThoai"].ToString());
            return RedirectToAction("DanhSaDanhSachSoDienThoai","Home");
        }
        [Authorize(Roles = "DanhSach")]
        public ActionResult DanhSaDanhSachSoDienThoai()
        {
            try
            {
                var ds = data.QuanLyTheoSoDienThoais.Where(n => n.SoDienThoaiQuanLy == Session["SoDienThoai"].ToString()).ToList();
                return View(ds);
            }catch(Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

        }
        [Authorize(Roles = "DanhSach")]
        public ActionResult ThemSoDienThoai(FormCollection collection)
        {
            var sodt = collection["ThemDienThoai"];
            var ds = data.QuanLyTheoSoDienThoais.Where(n => n.SoDienThoaiQuanLy == Session["SoDienThoai"].ToString()).ToList();
            if(data.QuanLyTheoSoDienThoais.Where(n=>n.SoDienThoaiQuanLy== Session["SoDienThoai"].ToString() && n.SoDienThoai == sodt).Count() == 0)
            {
                data.Ins_TaiKhoan_Quyen(sodt, Session["SoDienThoai"].ToString());
            }
            return RedirectToAction("DanhSaDanhSachSoDienThoai", "Home");
        }
        [Authorize(Roles = "DanhSach")]
        public ActionResult DanhSachNguoiCheckIn(int? page)
        {
            if(Session["IDDiemKiemDich"] is null)
            {
                return RedirectToAction("DanhSachDiemKiemDich","Home");
            }
            var danhsach = data.Get_Danhsach_TheoDiaDiem(int.Parse(Session["IDDiemKiemDich"].ToString())).ToList();
            //var danhsach = data.Get_Danhsach_TheoDiaDiem(int.Parse(Session["IDDiemKiemDich"].ToString())).ToList();
            int pageSize = 20;
            int pageNum = (page ?? 1);
            return View(danhsach.ToPagedList(pageNum, pageSize));
        }
        [Authorize(Roles = "DanhSach")]
        public ActionResult DanhSachNguoiCheckIn_TuNgay(int? page, FormCollection collection)
        {
            try
            {
                DateTime tungay = new DateTime();
                DateTime denngay = new DateTime();

                if (collection["TuNgay"] is null || collection["DenNgay"] is null)
                {
                    tungay = DateTime.Parse(String.Format("{0:MM/dd/yyyy HH:mm}", Session["TuNgay"].ToString()));
                    denngay = DateTime.Parse(String.Format("{0:MM/dd/yyyy HH:mm}", Session["DenNgay"].ToString()));
                }
                else
                {
                    tungay = DateTime.Parse(String.Format("{0:MM/dd/yyyy HH:mm}", collection["TuNgay"]));
                    denngay = DateTime.Parse(String.Format("{0:MM/dd/yyyy HH:mm}", collection["DenNgay"]));
                    ViewData["TuNgay"] = collection["TuNgay"];
                    ViewData["DenNgay"] = collection["DenNgay"];
                    Session["TuNgay"] = collection["TuNgay"];
                    Session["DenNgay"] = collection["DenNgay"];
                }

                var danhsach = data.Get_Danhsach_TheoDiaDiem_Search(int.Parse(Session["IDDiemKiemDich"].ToString()), tungay, denngay).ToList();
                //var danhsach = data.Get_Danhsach_TheoDiaDiem(int.Parse(Session["IDDiemKiemDich"].ToString())).ToList();
                int pageSize = 20;
                int pageNum = (page ?? 1);
                return View(danhsach.ToPagedList(pageNum, pageSize));
            }catch(Exception ex)
            {
                return RedirectToAction("Login", "Home");
            }

           }
        [Authorize(Roles = "DanhSach")]
        public ActionResult QRCode_DiaDiem(int? id)
        {
            if(id != null)
            {
                Session["IDDiemKiemDich"] = id;
            }
            var thongtin = data.DiemKiemDiches.FirstOrDefault(n => n.IdDiemKiemDich == int.Parse(Session["IDDiemKiemDich"].ToString()));
            Session["TenDiemKiemDich"] = thongtin.TenDiemKiemDich;
            string encodedStrTen = Convert.ToBase64String(Encoding.UTF8.GetBytes(thongtin.TenDiemKiemDich));
            string urlparam = thongtin.IdDiemKiemDich + "&tendiadiem=" + encodedStrTen;
            //string decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(encodedStr));

            Url generator = new Url("https://1022.tayninh.gov.vn//vi/tai-app?id="+ urlparam);
            //string payload = generator.ToString();
            string payload = "https://1022.tayninh.gov.vn//vi/tai-app?id=" + urlparam;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(payload, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            var qrCodeAsBitmap = qrCode.GetGraphic(20);

            ImageConverter converter = new ImageConverter();
            ViewBag.IMG = (byte[])converter.ConvertTo(qrCodeAsBitmap, typeof(byte[]));
            return View();
        }
        public ActionResult Header()
        {
            return PartialView();
        }
        public ActionResult Image(Bitmap qr)
        {
            var bitmapBytes = BitmapToBytes(qr); //Convert bitmap into a byte array
            return File(bitmapBytes, "image/jpeg"); //Return as file result
        }

        // This method is for converting bitmap into a byte array
        private static byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            //xoa add cookie
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddSeconds(1);
            }
            Session["SoDienThoai"] = null;
            Session["IDDiemKiemDich"] = null;
            Session["TenDiemKiemDich"] = null;
            return RedirectToAction("Login");
        }
        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            var sodienthoai = collection["mobilenumber"];
            Random rnd = new Random();
            int otp = rnd.Next(100000, 999999);

            var check = data.ChekOTPTime(sodienthoai).FirstOrDefault();
            if(check != null){
                if(check.TimeOTP > 3)
                {
                    data.Ins_TaiKhoan(sodienthoai, otp.ToString());
                    //Gửi OTP
                    //guitinmobi(sodienthoai, otp.ToString() + " la ma xac thuc dang nhap. Ma OTP se het han trong 3 phut");
                }
            }
            else
            {
                data.Ins_TaiKhoan(sodienthoai, otp.ToString());
                //Gửi OTP
                guitinmobi(sodienthoai, otp.ToString() + " la ma xac thuc dang nhap. Ma OTP se het han trong 3 phut");
            }

            data.Ins_TaiKhoan_Quyen(sodienthoai, sodienthoai);

            //Gửi OTP
            guitinmobi(sodienthoai, otp.ToString() + " la ma xac thuc dang nhap. Ma OTP se het han trong 3 phut");

            return RedirectToAction("XacThuc","Home", new { sodienthoai  = sodienthoai });
        }
        public void guitinmobi(string sdt, string nd)
        {
            var client = new RestClient(API);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "text/xml");
            request.AddParameter("text/xml", "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:send=\"http://send_sms.vienthongdidong.vn/\">\r\n   <soapenv:Header/>\r\n   <soapenv:Body>\r\n      <send:send>\r\n         <!--Optional:-->\r\n         <USERNAME>"+ taikhoansms + "</USERNAME>\r\n         <!--Optional:-->\r\n         <PASSWORD>"+ matkhausms + "</PASSWORD>\r\n         <!--Optional:-->\r\n         <BRANDNAME>"+ brand + "</BRANDNAME>\r\n         <!--Optional:-->\r\n         <MESSAGE>" + nd + "</MESSAGE>\r\n         <!--Optional:-->\r\n         <TYPE>1</TYPE>\r\n         <!--Optional:-->\r\n         <PHONE>" + sdt + "</PHONE>\r\n         <!--Optional:-->\r\n         <IDREQ>100000001</IDREQ>\r\n      </send:send>\r\n   </soapenv:Body>\r\n</soapenv:Envelope>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            // Console.WriteLine(response.Content);

        }
        public ActionResult XacThuc(string sodienthoai)
        {
            ViewBag.SoDienThoai= sodienthoai;
            return View();
        }
        [HttpPost]
        public ActionResult XacThuc(FormCollection collection)
        {
            var sdt = collection["sodienthoai"]; ;
            var otp = collection["otp"]; ;
            var taikhoan = data.TaiKhoans.Where(n => n.SoDienThoai == sdt && n.OTP == otp);
            if (taikhoan.Count() > 0)
            {
                var authTicket = new FormsAuthenticationTicket(
                1,                             // version
                taikhoan.FirstOrDefault().SoDienThoai,                      // user name
                DateTime.Now,                  // created
                DateTime.Now.AddDays(30),   // expires
                true,                    // persistent?
                "DanhSach"                        // can be used to store roles
                );
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                System.Web.HttpContext.Current.Response.Cookies.Add(authCookie);
                Session["SoDienThoai"] = taikhoan.FirstOrDefault().SoDienThoai;
                Session["TenDiemKiemDich"] = "";
                return RedirectToAction("DanhSachDiemKiemDich","Home");
            }
            ViewBag.ThongBao = "Nhập sai mã xác thực!";
            ViewBag.SoDienThoai = sdt;
            return View();
        }
    }
}