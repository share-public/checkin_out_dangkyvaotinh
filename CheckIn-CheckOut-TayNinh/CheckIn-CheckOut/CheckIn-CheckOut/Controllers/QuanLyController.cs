﻿using CheckIn_CheckOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CheckIn_CheckOut.Controllers
{
    public class QuanLyController : Controller
    {
        DataConnectDataContext data = new DataConnectDataContext();
        // GET: QuanLy
        [Authorize(Roles = "TongHop")]
        public ActionResult Index()
        {
            var ds= data.ThongKeSoluongnguoi("In", DateTime.Now.AddYears(-1), DateTime.Now).ToList();
            return View(ds);
        }
        [Authorize(Roles = "TongHop")]
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            var loai = collection["Loai"];
            var tungay = DateTime.Parse(String.Format("{0:MM/dd/yyyy HH:mm}", collection["TuNgay"]));
            var denngay = DateTime.Parse(String.Format("{0:MM/dd/yyyy HH:mm}", collection["DenNgay"]));
            var ds = data.ThongKeSoluongnguoi(loai, tungay, denngay).ToList();
            ViewBag.TuNgay = collection["TuNgay"];
            ViewBag.DenNgay = collection["DenNgay"];
            return View(ds);
        }

        // GET: QuanLy/Details/5
        public ActionResult Details(int id)
        {
            var ct = data.GetThongTin_DiaDiem(id).FirstOrDefault();
            return PartialView(ct);
        }

        // GET: QuanLy/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QuanLy/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QuanLy/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: QuanLy/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QuanLy/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: QuanLy/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            //xoa add cookie
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddSeconds(1);
            }
            Session["SoDienThoai"] = null;
            Session["IDDiemKiemDich"] = null;
            Session["TenDiemKiemDich"] = null;
            return RedirectToAction("Login");
        }
        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            var user = collection["USER"];
            var pass = collection["PASS"];
            var taikhoan = data.TaiKhoans.Where(n => n.username == user && n.password == pass);
            if (taikhoan.Count() > 0)
            {
                var authTicket = new FormsAuthenticationTicket(
                1,                             // version
                taikhoan.FirstOrDefault().SoDienThoai,                      // user name
                DateTime.Now,                  // created
                DateTime.Now.AddDays(30),   // expires
                true,                    // persistent?
                taikhoan.FirstOrDefault().quyen                      // can be used to store roles
                );
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                System.Web.HttpContext.Current.Response.Cookies.Add(authCookie);
                Session["SoDienThoai"] = taikhoan.FirstOrDefault().SoDienThoai;
                return RedirectToAction("Index", "QuanLy");
            }
            ViewBag.ThongBao = "Nhập sai mã xác thực!";
            return View();
        }
    }
}
