﻿using CheckIn_CheckOut.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckIn_CheckOut.Controllers
{
    public class KhaiBaoDiChuyenController : Controller
    {
        String tokenZalo = ConfigurationManager.AppSettings["ToKenkeyZalo"];
        public class Infomation
        {
            public int error { get; set; }
            public string message { get; set; }
            public dataInfo data { get; set; }
        }
        public class dataInfo
        {
            public int user_gender { get; set; }
            public long user_id { get; set; }
            public long user_id_by_app { get; set; }
            public avatars avatars { get; set; }
            public string avatar { get; set; }
            public string display_name { get; set; }
            public int birth_date { get; set; }
            public string shared_info { get; set; }
        }
        public class avatars
        {
            public string avatar120 { get; set; }
            public string avatar240 { get; set; }
        }
        DataConnectDataContext data = new DataConnectDataContext();
        [Route("nhap-thong-tin/{id}")]
        public ActionResult NhapThongTin(int id)
        {
            ViewData["TinhDiChuyen"] = new SelectList(data.TinhThanhs.Where(n => n.IDTinhTrang == 0).OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
            ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");
            ViewData["ChotKiemDich"] = new SelectList(data.ChotKiemTraVaos.ToList(), "IDChot", "TenChot",id);
            ViewBag.TinhThanh = data.TinhThanhs.Where(n => n.IDTinhTrang == 0).OrderBy(n => n.Stt).ToList();

            return View();
        }
        [HttpPost]
        [Route("nhap-thong-tin/{id}")]
        public ActionResult NhapThongTin(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                //ViewData["Tinh"] = new SelectList(data.TinhThanhs.OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
                //ViewData["LoaiDoiTuong"] = new SelectList(data.LoaiDoiTuongs.ToList().OrderBy(n => n.IDLoaiDoiTuong), "IDLoaiDoiTuong", "TenLoai");
                //ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");

                var sodienthoai = collection["SoDienThoai"].ToString();
                var hoten = collection["HoTen"];
                var cmnd = collection["CMND"];
                string titleCase = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(hoten.ToLower());
                //tỉnh di chuyển đến bên ngoài tỉnh tây Ninh
                var idtinh = int.Parse(collection["TINH_HIEN_TAI"]);
                var idquan_dichuyen = int.Parse(collection["HUYEN_HIEN_TAI"]);
                var idxa_dichuyen = int.Parse(collection["XA_HIEN_TAI"]);

                var diemden_trongngay = collection["diadiem"];
                var thoigiannhap = collection["ThoiGianNhap"];

                // thông tin id tinh huyen xa ap,  trong tỉnh
                var idxa = 0;
                try
                {
                    idxa = int.Parse(collection["ListDonVi"]);
                }
                catch
                {
                    idxa = 0;
                }
                var idhuyen = 0;
                try
                {
                    idhuyen = int.Parse(collection["Huyen"]);
                }
                catch
                {
                    idhuyen = 0;
                }
                var idap = 0;
                try
                {
                    idap = int.Parse(collection["AP_HIEN_TAI"]);
                }
                catch
                {

                }
                var diachi = collection["DiaChi"];
                var oaid = collection["OAID"];
                var iddiemkiemdich = int.Parse(collection["ChotKiemDich"]);

                //khai hộ
                //var khaiho = collection["khaiho"];
                // mac định khai hộ bằng 1/
                var khaiho = 1;
                string hanhdongkhai = "[KHAI HỘ] ";
                Boolean tempValue = collection["khaiho"] != null ? true : false;
                if (tempValue == true)
                {
                    khaiho = 1;
                    hanhdongkhai = "[KHAI HỘ] ";
                }
                var loaikhaibao = int.Parse(collection["LoaiLT"]);
                string mucdich = "";
                //Loai =0 la di trong ngya
                //Loai ==1 luu tru lau ngay
                if (loaikhaibao == 0)
                {
                    mucdich = "Mục đích khai báo: Di chuyển trong ngày!";
                    //data.Ins_KhaiBao_ThemLoai(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, null, idxa, null, oaid, idtinh, diachi,idhuyen, loaikhaibao,null);
                    //data.Ins_KhaiBao_ThemLoaiV2(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, cmnd, idxa, null, oaid, idtinh, diemden_trongngay, idhuyen, loaikhaibao, null, idquan_dichuyen, idxa_dichuyen);
                    data.Ins_KhaiBao_V3(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, cmnd, idxa, null, oaid, idtinh, diemden_trongngay, idhuyen, loaikhaibao, null, idquan_dichuyen, idxa_dichuyen, thoigiannhap,"NhapTaiChot",null,null,null,null);
                }
                else
                {
                    mucdich = "Mục đích khai báo: Về nơi cư trú !";
                    //data.Ins_KhaiBao_ThemLoai(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, null, idxa, null, oaid, idtinh, diachi, null, loaikhaibao, null);
                    data.Ins_KhaiBao_ThemLoaiV2(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, cmnd, idxa, null, oaid, idtinh, diachi, null, loaikhaibao, idap, idquan_dichuyen, idxa_dichuyen);
                    data.Ins_KhaiBao_V3(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, cmnd, idxa, null, oaid, idtinh, diachi, null, loaikhaibao, idap, idquan_dichuyen, idxa_dichuyen,thoigiannhap, "NhapTaiChot", null, null, null, null);
                    //data.Ins_KhaiBao(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", 0, titleCase, null, idxa, null, oaid, idtinh,diachi);
                }


                // inset to khai y tế

                var cau1 = int.Parse(collection["Cau1"]);
                var cau2 = int.Parse(collection["Cau2"]);
                var cau3 = int.Parse(collection["Cau3"]);
                var cau4 = int.Parse(collection["Cau4"]);
                data.Ins_ToKhaiYTe(titleCase, sodienthoai, cau1, cau2, cau3, cau4, iddiemkiemdich, null, null, khaiho, "DANGKY");

                //Gửi thông tin cho cán bộ y tế nếu có người khai báo có

                //lấy danh sach thong tin người kiem dich

                //gửi Thong báo nếu có người khai báo có



                //Get thong tin thong báo
                var ttTinh = data.TinhThanhs.SingleOrDefault(n => n.IDTinhThanh == idtinh);
                var ttXa = data.XaPhuongs.SingleOrDefault(n => n.IdXaPhuong == idxa);
                var tthuyen = data.QuanHuyens.SingleOrDefault(n => n.IDQuanHuyen == idhuyen);
                var ttchot = data.ChotKiemTraVaos.SingleOrDefault(n => n.IDChot == iddiemkiemdich);
                //Tính gio
                //var khoangcach = GetDistance(double.Parse(ttXa.QuanHuyen.Longitude), double.Parse(ttXa.QuanHuyen.Latitude), double.Parse(ttchot.Longitude), double.Parse(ttchot.Latitude));

                TinhGioDiChuyen getGio = new TinhGioDiChuyen();
                List<PhuTrachDiaBan> dsphutrach = new List<PhuTrachDiaBan>();

                // tạo biến để chek gửi mess trng ngày
                int trongngay = 0;

                //Loai =0 la di trong ngya
                //Loai ==1 luu tru lau ngay
                if (loaikhaibao == 0)
                {
                    trongngay = 0;
                    getGio = data.TinhGioDiChuyens.FirstOrDefault(n => n.idchot == iddiemkiemdich & n.idhuyen == idhuyen);
                    dsphutrach = data.PhuTrachDiaBans.Where(n => n.IDHuyen == idhuyen && n.ThongBao == true).ToList();
                }
                else
                {
                    trongngay = 1;
                    getGio = data.TinhGioDiChuyens.FirstOrDefault(n => n.idchot == iddiemkiemdich & n.idhuyen == ttXa.QuanHuyen.IDQuanHuyen);
                    dsphutrach = data.PhuTrachDiaBans.Where(n => n.IDXa == idxa & n.ThongBao == true).ToList();
                    var chekidhuyen = data.XaPhuongs.SingleOrDefault(n => n.IdXaPhuong == idxa).IDQuanHuyen;
                    var dshuyen = data.PhuTrachDiaBans.Where(n => n.IDHuyen == chekidhuyen & n.ThongBao == true).ToList();
                    dsphutrach = dsphutrach.Concat(dshuyen).ToList();
                }


                TimeSpan ts = TimeSpan.FromMinutes(double.Parse(getGio.giodichuyen.ToString()));
                //TimeSpan ts = TimeSpan.FromMinutes(180);
                var hours = ts.Hours;
                var minutes = ts.Minutes;
                string gio = "0";
                if (hours > 0)
                {
                    gio = String.Format("{0} giờ {1} phút", hours, minutes);
                }
                else
                {
                    gio = String.Format("{0} phút", minutes);
                }
                var ngay = DateTime.Now.AddMinutes(double.Parse(getGio.giodichuyen.ToString())).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);


                string tb = hanhdongkhai + "Cảm ơn Ông/Bà " + titleCase + " đã thực hiện khai báo. Thời gian di chuyển dự kiến của Ông/Bà là trong " + gio + ". (Trong ngày " + ngay + " ) Ông/Bà phải thực hiện xác nhận đã đến nơi khi tới địa điểm khai báo.Hãy đưa thông báo này cho cán bộ tại chốt kiểm soát. " + mucdich + "\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* CMND/CCCD: " + cmnd;

                //Tắt thông báo khai hộ cán bộ

                ////check oaid
                //if (oaid == "0" || oaid == "")
                //{
                //    //Lấy userid zalo
                //    string urlgetuser = "https://openapi.zalo.me/v2.0/oa/getprofile?access_token="+tokenZalo+"&data={%22user_id%22=%22" + sodienthoai + "%22}";
                //    var webClient = new System.Net.WebClient();
                //    var jsoninforamtion = webClient.DownloadString(urlgetuser);
                //    var messageinfo = JsonConvert.DeserializeObject<Infomation>(jsoninforamtion);
                //    SentZaloMess(messageinfo.data.user_id.ToString(), tb, sodienthoai);
                //}
                //else
                //{
                //    SentZaloMess(oaid, tb, sodienthoai);
                //}


                //Get danh sach người phụ trach gửi thong báo
                //Gui thong bao cho danh sach nhan
                if (dsphutrach.Count() > 0)
                {

                    foreach (var item in dsphutrach)
                    {
                        try
                        {
                            if (item.OaID is null || item.OaID == "")
                            {
                                //Lấy userid zalo
                                string urlgetuser = "https://openapi.zalo.me/v2.0/oa/getprofile?access_token=" + tokenZalo + "&data={%22user_id%22=%22" + item.DienThoai + "%22}";
                                var webClient = new System.Net.WebClient();
                                var jsoninforamtion = webClient.DownloadString(urlgetuser);
                                var messageinfo = JsonConvert.DeserializeObject<Infomation>(jsoninforamtion);
                                //check người dùng có quan tâm OA ko
                                if (messageinfo.error == 0)
                                {
                                    data.CapNhatOAID_NguoiPhuyTrach(item.DienThoai, messageinfo.data.user_id.ToString());
                                    if (idxa == 0)
                                    {

                                        //Tin nhắn về trong ngày
                                        string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + tthuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + "." + mucdich + "\\n----- Thông tin khai báo -----\\n* CMND/CCCD: " + cmnd;
                                        if (trongngay == 1)
                                        {
                                            SentZaloMess(messageinfo.data.user_id.ToString(), thongbao, item.DienThoai);
                                        }
                                    }
                                    else
                                    {
                                        string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + ttXa.TenXaPhuong + ", " + ttXa.QuanHuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + "." + mucdich + "\\n----- Thông tin khai báo -----\\n* CMND/CCCD: " + cmnd;
                                        if (trongngay == 1)
                                        {
                                            SentZaloMess(messageinfo.data.user_id.ToString(), thongbao, item.DienThoai);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (idxa == 0)
                                {
                                    string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + tthuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + ". " + mucdich + "\\n----- Thông tin khai báo -----\\n* CMND/CCCD: " + cmnd;
                                    if (trongngay == 1)
                                    {
                                        SentZaloMess(item.OaID, thongbao, item.DienThoai);
                                    }
                                }
                                else
                                {
                                    string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + ttXa.TenXaPhuong + ", " + ttXa.QuanHuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + ". " + mucdich + "\\n----- Thông tin khai báo -----\\n* CMND/CCCD: " + cmnd;
                                    if (trongngay == 1)
                                    {
                                        SentZaloMess(item.OaID, thongbao, item.DienThoai);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }

                }
                return RedirectToAction("NhapThongTin", "KhaiBaoDiChuyen", new { id= iddiemkiemdich });
            }
            catch (Exception ex)
            {
                ViewData["TinhDiChuyen"] = new SelectList(data.TinhThanhs.Where(n => n.IDTinhTrang == 0).OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
                ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");
                ViewData["ChotKiemDich"] = new SelectList(data.ChotKiemTraVaos.ToList(), "IDChot", "TenChot");
                ViewBag.TinhThanh = data.TinhThanhs.Where(n => n.IDTinhTrang == 0).OrderBy(n => n.Stt).ToList();

                ViewBag.ThongBaoLoi = ex.Message;
                return View();
            }
        }
        // GET: KhaiBaoDiChuyen
        public ActionResult Index()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetThongTinCu(string oaid)
        {
            dataInfo res = new dataInfo();
            if (string.IsNullOrEmpty(oaid))
            {
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            var tt = data.Get_ThongTin_CaNhan(oaid).FirstOrDefault();
            if (tt is null)
            {
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            res.display_name = tt.HoTen;
            res.shared_info = tt.SoDienThoai;
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getQuanhuyen(string IDTINH)
        {
            if (string.IsNullOrEmpty(IDTINH))
                return Json(HttpNotFound());
            var categoryList = data.QuanHuyens.Where(n => n.IDTinhThanh == (Convert.ToInt32(IDTINH))).ToList();
            var categoryData = categoryList.Select(m => new SelectListItem()
            {
                Text = m.TenQuanHuyen,
                Value = m.IDQuanHuyen.ToString()
            });
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getAp(string IDXa)
        {
            if (string.IsNullOrEmpty(IDXa))
                return Json(HttpNotFound());
            var categoryList = data.Aps.Where(n => n.IDXaPhuong == (Convert.ToInt32(IDXa))).ToList();
            var categoryData = categoryList.Select(m => new SelectListItem()
            {
                Text = m.TenAp,
                Value = m.IDAp.ToString()
            });
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getTinhThanh()
        {
            try
            {
                var categoryList = data.TinhThanhs.OrderBy(n => n.Stt).ToList();
                var categoryData = categoryList.Select(m => new SelectListItem()
                {
                    Text = m.TenTinhThanh,
                    Value = m.IDTinhThanh.ToString()
                });
                return Json(categoryData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(HttpNotFound());
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult getPhuongxa(string IDHUYEN)
        {
            try
            {

                var tinhdsad = IDHUYEN;
                var categoryList = data.XaPhuongs.Where(n => n.IDQuanHuyen == Convert.ToInt32(IDHUYEN)).ToList();
                var categoryData = categoryList.Select(m => new SelectListItem()
                {
                    Text = m.TenXaPhuong,
                    Value = m.IdXaPhuong.ToString()
                });
                return Json(categoryData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(HttpNotFound());
            }
        }

        // GET: KhaiBaoDiChuyen/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        // GET: KhaiBaoDiChuyen/Create
        [Route("dang-ky-di-chuyen")]
        public ActionResult KhaiBao()
        {
            ViewData["TinhDiChuyen"] = new SelectList(data.TinhThanhs.Where(n=>n.IDTinhTrang == 0).OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
            ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");
            ViewData["ChotKiemDich"] = new SelectList(data.ChotKiemTraVaos.ToList(), "IDChot", "TenChot");
            ViewBag.TinhThanh = data.TinhThanhs.Where(n => n.IDTinhTrang == 0).OrderBy(n => n.Stt).ToList();

            return View();
        }
        [Route("khai-bao-y-te")]
        public ActionResult KhaiBaoYTe()
        {
            ViewData["TinhDiChuyen"] = new SelectList(data.TinhThanhs.OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
            ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");
            return View();
        }
        [HttpPost]
        [Route("khai-bao-y-te")]
        public ActionResult KhaiBaoYTe(FormCollection collection)
        {
            try { 
            
            var sodienthoai = collection["SoDienThoai"].ToString();
            var hoten = collection["HoTen"];
            string titleCase = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(hoten.ToLower());
            var idxa = 0;
            try
            {
                idxa = int.Parse(collection["ListDonVi"]);
            }
            catch
            {
                idxa = 0;
            }
            var khaiho = 0;
            string hanhdongkhai = "";
            Boolean tempValue = collection["khaiho"] != null ? true : false;
            if (tempValue == true)
            {
                khaiho = 1;
                hanhdongkhai = "[KHAI HỘ] ";
            }
            // inset to khai y tế
            var cau1 = int.Parse(collection["Cau1"]);
            var cau2 = int.Parse(collection["Cau2"]);
            var cau3 = int.Parse(collection["Cau3"]);
            var cau4 = int.Parse(collection["Cau4"]);
            var longitude = collection["longitude"];
            var latitude = collection["latitude"];
            var OAID = collection["OAID"];
            //NEU KHAI HANG NGAY CẬP NHẬT IDXA VÀO CỘT CHỐT;
            data.Ins_ToKhaiYTe(titleCase, sodienthoai,cau1,cau2,cau3,cau4, idxa, latitude, longitude, khaiho, "HANGNGAY");

            string tb = hanhdongkhai + "Cảm ơn Ông/Bà đã thực hiện khai báo y tế hằng ngày. Thông tin người khai báo: "+ titleCase+" SĐT:"+ sodienthoai;
            SentZaloMess(OAID, tb, sodienthoai);

                //List<PhuTrachDiaBan> dsphutrach = new List<PhuTrachDiaBan>();
                //dsphutrach = data.PhuTrachDiaBans.Where(n => n.IDXa == idxa).ToList();

            return RedirectToAction("ThanhCong", "KhaiBaoDiChuyen");
            }
            catch (Exception ex)
            {
                ViewData["TinhDiChuyen"] = new SelectList(data.TinhThanhs.OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
                ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");
                ViewBag.ThongBaoLoi = ex.Message;
                return View();
            }
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult XacNhan(string oaid,string latitude, string longitude)
        {
            if (string.IsNullOrEmpty(latitude))
                return Json(HttpNotFound());
            //var tt = data.Get_ThongTin_CaNhan(oaid).FirstOrDefault();
            var tt = data.Get_ThongTin_CaNhan_v2(oaid).FirstOrDefault();
            
            if(tt != null)
            {
                //data.Ins_XacNhanViTri(tt.IDDiemKiemDich, tt.SoDienThoai, DateTime.Now, "XNTD", 0, tt.HoTen, null, tt.IDXa, null, oaid, tt.IDTinhDi, latitude + "|" + longitude, null, null, tt.DiaCHi);
                data.Ins_XacNhanViTri_V2(tt.IDDiemKiemDich, tt.SoDienThoai, DateTime.Now, "XNTD", 0, tt.HoTen, null, tt.IDXa, null, oaid, tt.IDTinhDi, latitude + "|" + longitude, null, null, tt.DiaCHi,tt.IDHuyen);
                
                //Get thong tin thong báo
                var ttTinh = data.TinhThanhs.SingleOrDefault(n => n.IDTinhThanh == tt.IDTinhDi);
                var ttXa = data.XaPhuongs.SingleOrDefault(n => n.IdXaPhuong == tt.IDXa);
                
                string tbcanhan = "Ông/Bà đã thực hiện xác nhận điểm đến thành công. Hãy cài đặt ứng dụng Tây Ninh Smart để thực hiện xác nhận đang ở địa điểm khai báo hàng ngày.";
                SentZaloMess(oaid, tbcanhan, tt.SoDienThoai);
                string tb = "Xác nhận đã đến: Ông/Bà " + tt.HoTen + ", số điện thoại " + tt.SoDienThoai + ", từ " + ttTinh.TenTinhThanh + " đăng ký về địa chỉ " + tt.DiaCHi + ", " + ttXa.TenXaPhuong + ", " + ttXa.QuanHuyen.TenQuanHuyen + " đã đến nơi.Xem vị trí tại: https://www.google.com/maps?q=" + latitude + "," + longitude + "&z=14&t=m&mapclient=embed";
                //Get danh sach người phụ trach gửi thong báo
                var dsphutrach = data.PhuTrachDiaBans.Where(n => n.IDXa == tt.IDXa).ToList();
                if (dsphutrach.Count() > 0)
                {
                    foreach (var item in dsphutrach)
                    {
                        if (item.OaID is null)
                        {
                            //Lấy userid zalo
                            string urlgetuser = "https://openapi.zalo.me/v2.0/oa/getprofile?access_token=" + tokenZalo + "&data={%22user_id%22=%22" + item.DienThoai + "%22}";
                            var webClient = new System.Net.WebClient();
                            var jsoninforamtion = webClient.DownloadString(urlgetuser);
                            var messageinfo = JsonConvert.DeserializeObject<Infomation>(jsoninforamtion);
                            data.CapNhatOAID_NguoiPhuyTrach(item.DienThoai, messageinfo.data.user_id.ToString());
                            SentZaloMess(messageinfo.data.user_id.ToString(), tb, item.DienThoai);
                        }
                        else
                        {
                            SentZaloMess(item.OaID, tb,item.DienThoai);
                        }
                    }

                }
                return Json("Thành Công!", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(HttpNotFound());
            }

        }
        public void SaveImage(string base64image, string name, string path)
        {
            if (string.IsNullOrEmpty(base64image))
                return;

            var t = base64image.Substring(base64image.IndexOf(",")+1);  // remove data:image/png;base64,

            byte[] bytes = Convert.FromBase64String(t);

            //Image image;
            //using (MemoryStream ms = new MemoryStream(bytes))
            //{
            //    image = Image.FromStream(ms);
            //}
            using (var ms = new MemoryStream(bytes))
            {
                using (var bitmap = new Bitmap(ms))
                {
                    float width = 1027;
                    float height = 768;

                    var rawImage = Image.FromStream(ms);
                    float scale = Math.Min(width / rawImage.Width, height / rawImage.Height);
                    var scaleWidth = (int)(rawImage.Width * scale);
                    var scaleHeight = (int)(rawImage.Height * scale);

                    var bitmapCopy = new Bitmap(bitmap, scaleWidth, scaleHeight);
                    //var brush = new SolidBrush(Color.White);

                    //Graphics graph = Graphics.FromImage(bitmapCopy);
                    //graph.InterpolationMode = InterpolationMode.High;
                    //graph.CompositingQuality = CompositingQuality.HighQuality;
                    //graph.SmoothingMode = SmoothingMode.AntiAlias;
                    //graph.FillRectangle(brush, new RectangleF(0, 0, width, height));
                    //graph.DrawImage(rawImage, new Rectangle(0, 0, scaleWidth, scaleHeight));

                    //return bitmapCopy;
                    //var randomFileName = Guid.NewGuid().ToString().Substring(0, 4) + ".png";
                    var fullPath = Path.Combine(Server.MapPath(path), name);
                    bitmapCopy.Save(fullPath, System.Drawing.Imaging.ImageFormat.Png);


                }
            }

        }
        // POST: KhaiBaoDiChuyen/Create
        public ActionResult Ver()
        {
            ViewBag.Ver = "12.07.09";
            return PartialView();
        }
        [HttpPost]
        [Route("dang-ky-di-chuyen")]
        public ActionResult KhaiBao(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                //ViewData["Tinh"] = new SelectList(data.TinhThanhs.OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
                //ViewData["LoaiDoiTuong"] = new SelectList(data.LoaiDoiTuongs.ToList().OrderBy(n => n.IDLoaiDoiTuong), "IDLoaiDoiTuong", "TenLoai");
                //ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");

                var sodienthoai = collection["SoDienThoai"].ToString();
                var hoten = collection["HoTen"];
                var cmnd = collection["CMND"];
                string titleCase = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(hoten.ToLower());
                //tỉnh di chuyển đến bên ngoài tỉnh tây Ninh
                var idtinh = int.Parse(collection["TINH_HIEN_TAI"]);
                var idquan_dichuyen = int.Parse(collection["HUYEN_HIEN_TAI"]);
                var idxa_dichuyen = int.Parse(collection["XA_HIEN_TAI"]);

                var diemden_trongngay = collection["diadiem"];

                // thông tin id tinh huyen xa ap,  trong tỉnh
                var idxa = 0;
                try
                {
                   idxa = int.Parse(collection["ListDonVi"]);
                }
                catch
                {
                   idxa = 0;
                }
                var idhuyen = 0;
                try
                {
                    idhuyen = int.Parse(collection["Huyen"]);
                }
                catch
                {
                    idhuyen = 0;
                }
                var idap = 0;
                string tenap = "";
                try
                {
                    idap = int.Parse(collection["AP_HIEN_TAI"]);
                    tenap = data.Aps.FirstOrDefault(n => n.IDAp == idap).TenAp;
                }
                catch
                {

                }
                var diachi = collection["DiaChi"];
                var oaid = collection["OAID"];
                var iddiemkiemdich = int.Parse(collection["ChotKiemDich"]);



                var cmndtruoc = collection["CMNDTruoc"];
                var cmndsau = collection["CMNDSau"];
                var cmndxetnghiem = collection["GiayXetNghiem"];
                string ngaythang = DateTime.Now.Ticks.ToString();
                var filename = ngaythang + "_" + Guid.NewGuid().ToString() + ".png";
                string urlhinh = filename;
                SaveImage(cmndtruoc, urlhinh, "~/LuuThongTin/CMNDTRUOC");

                var filename2 = ngaythang + "_" + Guid.NewGuid().ToString() + ".png";
                string urlhinh2 = filename2;
                SaveImage(cmndsau, urlhinh, "~/LuuThongTin/CMNDSAU");



                string hinhxetnghiem = "";
                if (cmndxetnghiem != null)
                {

                    var filename3 = ngaythang + "_" + Guid.NewGuid().ToString() + ".png";
                    SaveImage(cmndxetnghiem, urlhinh, "~/LuuThongTin/PhieuXetNghiem");
                    hinhxetnghiem = filename3;
                }


                //khai hộ
                //var khaiho = collection["khaiho"];
                var khaiho = 0;
                string hanhdongkhai = "";
                Boolean tempValue = collection["khaiho"] != null ? true : false;
                if(tempValue == true)
                {
                    khaiho = 1;
                    hanhdongkhai = "[KHAI HỘ] ";
                }
                var loaikhaibao = int.Parse(collection["LoaiLT"]);
                string mucdich = "";
                //Loai =0 la di trong ngya
                //Loai ==1 luu tru lau ngay
                if (loaikhaibao == 0)
                {
                    mucdich = "Mục đích khai báo: Đi về trong ngày!";
                    //data.Ins_KhaiBao_ThemLoai(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, null, idxa, null, oaid, idtinh, diachi,idhuyen, loaikhaibao,null);
                    //data.Ins_KhaiBao_ThemLoaiV2(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, cmnd, idxa, null, oaid, idtinh, diemden_trongngay, idhuyen, loaikhaibao,null, idquan_dichuyen, idxa_dichuyen);
                    data.Ins_KhaiBao_V3(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, cmnd, idxa, null, oaid, idtinh, diemden_trongngay, idhuyen, loaikhaibao,null, idquan_dichuyen, idxa_dichuyen,null,null,null,urlhinh,urlhinh2,hinhxetnghiem);

                }
                else
                {
                    mucdich = "Mục đích khai báo: Về ở lại tại tỉnh Tây Ninh!";
                    //data.Ins_KhaiBao_ThemLoai(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, null, idxa, null, oaid, idtinh, diachi, null, loaikhaibao, null);
                    //data.Ins_KhaiBao_ThemLoaiV2(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, cmnd, idxa, null, oaid, idtinh, diachi, null, loaikhaibao, idap, idquan_dichuyen, idxa_dichuyen);
                    data.Ins_KhaiBao_V3(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", khaiho, titleCase, cmnd, idxa, null, oaid, idtinh, diachi, null, loaikhaibao, idap, idquan_dichuyen, idxa_dichuyen, null, null, null, urlhinh, urlhinh2, hinhxetnghiem);
                    //data.Ins_KhaiBao(iddiemkiemdich, sodienthoai, DateTime.Now, "KB", 0, titleCase, null, idxa, null, oaid, idtinh,diachi);
                }


                // inset to khai y tế

                var cau1 = int.Parse(collection["Cau1"]);
                var cau2 = int.Parse(collection["Cau2"]);
                var cau3 = int.Parse(collection["Cau3"]);
                var cau4 = int.Parse(collection["Cau4"]);
                data.Ins_ToKhaiYTe(titleCase, sodienthoai,cau1,cau2,cau3,cau4, iddiemkiemdich,null,null, khaiho, "DANGKY");

                string khaibaoyte = "\\n\\n****** THÔNG TIN KHAI BÁO YÊ TẾ ***** \\n";
                if (cau1 == 0 && cau2 == 0 & cau3 == 0 & cau4 == 0)
                    khaibaoyte = khaibaoyte + " - Bình thường";
                if (cau1 == 1)
                    khaibaoyte = khaibaoyte + " - CÓ dấu hiệu: sốt, ho, khó thở đâu họng, mệt mỏi trong vòng 14 ngày.\\n";
                if (cau2 == 1)
                    khaibaoyte = khaibaoyte + " - CÓ tiếp xúc với người bị nhiễm hoặc nghi ngờ nhiễm COVID-19 trong vòng 14 ngày.\\n";
                if(cau3==1)
                    khaibaoyte = khaibaoyte + " - CÓ di chuyển sang Tỉnh, Thành phố nào (có thể di chuyển qua nhiều nơi) khác nơi ở hiện tại trong vòng 14 ngày.\\n";
                if (cau4 == 1)
                    khaibaoyte = khaibaoyte + " - CÓ đang ở vùng dịch.\\n";
                //Gửi thông tin cho cán bộ y tế nếu có người khai báo có

                //lấy danh sach thong tin người kiem dich

                //gửi Thong báo nếu có người khai báo có



                //Get thong tin thong báo
                var ttTinh = data.TinhThanhs.SingleOrDefault(n => n.IDTinhThanh == idtinh);
                var ttXa = data.XaPhuongs.SingleOrDefault(n => n.IdXaPhuong == idxa);
                var tthuyen = data.QuanHuyens.SingleOrDefault(n => n.IDQuanHuyen == idhuyen);
                var ttchot = data.ChotKiemTraVaos.SingleOrDefault(n => n.IDChot == iddiemkiemdich);
                //Tính gio
                //var khoangcach = GetDistance(double.Parse(ttXa.QuanHuyen.Longitude), double.Parse(ttXa.QuanHuyen.Latitude), double.Parse(ttchot.Longitude), double.Parse(ttchot.Latitude));

                TinhGioDiChuyen getGio = new TinhGioDiChuyen();
                List<PhuTrachDiaBan> dsphutrach = new List<PhuTrachDiaBan>();
                List<PhuTrachDiaBan> dsphutrach_ap = new List<PhuTrachDiaBan>();

                // tạo biến để chek gửi mess trng ngày
                int trongngay = 0;

                //Loai =0 la di trong ngya
                //Loai ==1 luu tru lau ngay
                if (loaikhaibao == 0)
                {
                    trongngay = 0;
                    getGio = data.TinhGioDiChuyens.FirstOrDefault(n => n.idchot == iddiemkiemdich & n.idhuyen == idhuyen);
                    dsphutrach = data.PhuTrachDiaBans.Where(n => n.IDHuyen == idhuyen && n.ThongBao == true ).ToList();
                }
                else
                {
                    trongngay = 1;
                    getGio = data.TinhGioDiChuyens.FirstOrDefault(n => n.idchot == iddiemkiemdich & n.idhuyen == ttXa.QuanHuyen.IDQuanHuyen);
                    dsphutrach = data.PhuTrachDiaBans.Where(n => n.IDXa == idxa & n.ThongBao == true).ToList();
                    var chekidhuyen = data.XaPhuongs.SingleOrDefault(n => n.IdXaPhuong == idxa).IDQuanHuyen;
                    var dshuyen= data.PhuTrachDiaBans.Where(n => n.IDHuyen == chekidhuyen & n.ThongBao == true).ToList();
                    dsphutrach = dsphutrach.Concat(dshuyen).ToList();
                }


                TimeSpan ts = TimeSpan.FromMinutes(double.Parse(getGio.giodichuyen.ToString()));
                //TimeSpan ts = TimeSpan.FromMinutes(180);
                var hours = ts.Hours;
                var minutes = ts.Minutes;
                string gio = "0";
                if (hours > 0)
                {
                    gio=String.Format("{0} giờ {1} phút", hours, minutes);
                }
                else
                {
                    gio=String.Format("{0} phút", minutes);
                }
                var ngay = DateTime.Now.AddMinutes(double.Parse(getGio.giodichuyen.ToString())).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);


                //string tb = hanhdongkhai+ "Cảm ơn Ông/Bà " + titleCase + " đã thực hiện khai báo. " +
                //    "Thời gian di chuyển dự kiến của Ông/Bà là trong " + gio+". (Trong ngày "+ngay+" ) " +
                //    "Ông/Bà phải thực hiện xác nhận đã đến nơi khi tới địa điểm khai báo.Hãy đưa thông báo này cho cán bộ tại chốt kiểm soát. "
                //    + mucdich+
                //    "\\n----- Thông tin khai báo -----\\n* Họ và Tên: "+ titleCase + "\\n* CMND/CCCD: " + cmnd;

                string tb = hanhdongkhai + "Cảm ơn Ông/Bà " + titleCase + " đã thực hiện khai báo. " +
                    "Thời gian di chuyển dự kiến của Ông/Bà là trong " + gio + ". (Trong ngày " + ngay + " ) " +
                    "Ông/Bà phải thực hiện xác nhận đã đến nơi khi tới địa điểm khai báo.Hãy đưa thông báo này cho cán bộ tại chốt kiểm soát. "+
                    "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") +"\\n*" + mucdich;

                //check oaid
                if (oaid == "0" || oaid =="")
                {
                    try
                    {
                    //Lấy userid zalo
                    string urlgetuser = "https://openapi.zalo.me/v2.0/oa/getprofile?access_token=" + tokenZalo + "data={%22user_id%22=%22" + sodienthoai + "%22}";
                    var webClient = new System.Net.WebClient();
                    var jsoninforamtion = webClient.DownloadString(urlgetuser);
                    var messageinfo = JsonConvert.DeserializeObject<Infomation>(jsoninforamtion);
                    SentZaloMess(messageinfo.data.user_id.ToString(), tb, sodienthoai);
                }
                    catch (Exception ex)
                    {
                    // so dien thoai không chính xác
                    }

                }
                else
                {
                    SentZaloMess(oaid, tb, sodienthoai);
                }


                //Get danh sach người phụ trach gửi thong báo
                //Gui thong bao cho danh sach nhan
                if (dsphutrach.Count() > 0)
                {
                   
                    foreach(var item in dsphutrach)
                    {
                        try
                        {
                            if (item.OaID is null || item.OaID == "")
                            {
                                //Lấy userid zalo
                                string urlgetuser = "https://openapi.zalo.me/v2.0/oa/getprofile?access_token=" + tokenZalo + "&data={%22user_id%22=%22" + item.DienThoai + "%22}";
                                var webClient = new System.Net.WebClient();
                                var jsoninforamtion = webClient.DownloadString(urlgetuser);
                                var messageinfo = JsonConvert.DeserializeObject<Infomation>(jsoninforamtion);
                                //check người dùng có quan tâm OA ko
                                if(messageinfo.error == 0)
                                {
                                    data.CapNhatOAID_NguoiPhuyTrach(item.DienThoai, messageinfo.data.user_id.ToString());
                                    if (idxa == 0)
                                    {

                                        //Tin nhắn về trong ngày
                                        string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + tthuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + "." +
                                            "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "\\n*" + mucdich;
                                        if (trongngay == 1)
                                        {
                                            SentZaloMess(messageinfo.data.user_id.ToString(), thongbao + khaibaoyte, item.DienThoai);
                                            //SentZaloMess(messageinfo.data.user_id.ToString(), khaibaoyte, item.DienThoai);
                                        }
                                    }
                                    else
                                    {
                                        string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + tenap + ", " + ttXa.TenXaPhuong + ", " + ttXa.QuanHuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + "." +
                                            "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "\\n*" + mucdich; ;
                                        if (trongngay == 1)
                                        {
                                            SentZaloMess(messageinfo.data.user_id.ToString(), thongbao + khaibaoyte, item.DienThoai);
                                            //SentZaloMess(messageinfo.data.user_id.ToString(), khaibaoyte, item.DienThoai);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (idxa == 0)
                                {
                                    string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", "+ tthuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + ". " +
                                        "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "\\n*" + mucdich;
                                    if (trongngay == 1)
                                    {
                                        SentZaloMess(item.OaID, thongbao + khaibaoyte, item.DienThoai);
                                        //SentZaloMess(item.OaID, khaibaoyte, item.DienThoai);
                                    }
                                }
                                else
                                {
                                    string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi +", "+ tenap + ", " + ttXa.TenXaPhuong + ", " + ttXa.QuanHuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + ". " +
                                        "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "\\n*" + mucdich;
                                    if (trongngay == 1)
                                    {
                                        SentZaloMess(item.OaID, thongbao + khaibaoyte, item.DienThoai);
                                        //SentZaloMess(item.OaID, khaibaoyte, item.DienThoai);
                                    }
                                }
                            }
                        }
                        catch( Exception ex)
                        {

                        }

                    }

                }
                //Gửi thông báo cho ấp neu danhs ách >0
                if (dsphutrach_ap.Count() > 0)
                {
                    foreach (var item in dsphutrach_ap)
                    {
                        try
                        {
                            if (item.OaID is null || item.OaID == "")
                            {
                                //Lấy userid zalo
                                string urlgetuser = "https://openapi.zalo.me/v2.0/oa/getprofile?access_token=" + tokenZalo + "&data={%22user_id%22=%22" + item.DienThoai + "%22}";
                                var webClient = new System.Net.WebClient();
                                var jsoninforamtion = webClient.DownloadString(urlgetuser);
                                var messageinfo = JsonConvert.DeserializeObject<Infomation>(jsoninforamtion);
                                //check người dùng có quan tâm OA ko
                                if (messageinfo.error == 0)
                                {
                                    data.CapNhatOAID_NguoiPhuyTrach(item.DienThoai, messageinfo.data.user_id.ToString());
                                    if (idxa == 0)
                                    {

                                        //Tin nhắn về trong ngày
                                        string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + tthuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + "." +
                                            "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "\\n*" + mucdich;
                                        if (trongngay == 1)
                                        {
                                            SentZaloMess(messageinfo.data.user_id.ToString(), thongbao + khaibaoyte, item.DienThoai);
                                            //SentZaloMess(messageinfo.data.user_id.ToString(), khaibaoyte, item.DienThoai);
                                        }
                                    }
                                    else
                                    {
                                        string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + tenap + ", " + ttXa.TenXaPhuong + ", " + ttXa.QuanHuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + "." +
                                            "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "\\n*" + mucdich; ;
                                        if (trongngay == 1)
                                        {
                                            SentZaloMess(messageinfo.data.user_id.ToString(), thongbao + khaibaoyte, item.DienThoai);
                                            //SentZaloMess(messageinfo.data.user_id.ToString(), khaibaoyte, item.DienThoai);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (idxa == 0)
                                {
                                    string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + tthuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + ". " +
                                        "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "\\n*" + mucdich;
                                    if (trongngay == 1)
                                    {
                                        SentZaloMess(item.OaID, thongbao + khaibaoyte, item.DienThoai);
                                        //SentZaloMess(item.OaID, khaibaoyte, item.DienThoai);
                                    }
                                }
                                else
                                {
                                    string thongbao = "Thông báo: " + hanhdongkhai + "Ông/Bà: " + titleCase + ", số điện thoại " + sodienthoai + ", từ " + ttTinh.TenTinhThanh + " đã đăng ký về địa chỉ " + diachi + ", " + tenap + ", " + ttXa.TenXaPhuong + ", " + ttXa.QuanHuyen.TenQuanHuyen + ". Thời gian dự kiến đến nơi là " + gio + " ngày " + ngay + ". " +
                                        "\\n\\n----- Thông tin khai báo -----\\n* Họ và Tên: " + titleCase + "\\n* Thời gian khai báo: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "\\n*" + mucdich;
                                    if (trongngay == 1)
                                    {
                                        SentZaloMess(item.OaID, thongbao + khaibaoyte, item.DienThoai);
                                        //SentZaloMess(item.OaID, khaibaoyte, item.DienThoai);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                return RedirectToAction("ThanhCong", "KhaiBaoDiChuyen");
            }
            catch (Exception ex)
            {
                ViewData["TinhDiChuyen"] = new SelectList(data.TinhThanhs.Where(n => n.IDTinhTrang == 0).OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
                ViewData["Huyen"] = new SelectList(data.GetQuanHuyen(72).OrderBy(n => n.IDQuanHuyen), "IDQuanHuyen", "TenQuanHuyen");
                ViewData["ChotKiemDich"] = new SelectList(data.ChotKiemTraVaos.ToList(), "IDChot", "TenChot");
                ViewBag.TinhThanh = data.TinhThanhs.Where(n => n.IDTinhTrang == 0).OrderBy(n => n.Stt).ToList();

                //ViewBag.ThongBaoLoi = ex.Message;
                ViewBag.ThongBaoLoi = "Không thể gửi dữ liệu, vui lòng nhập lại!";
                return View();
            }
        }
        //[Route("dang-ky-thanh-cong")]

        public ActionResult XacNhanDen()
        {
            return View();
        }
        public double GetDistance(double longitude, double latitude, double otherLongitude, double otherLatitude)
        {
            var d1 = latitude * (Math.PI / 180.0);
            var num1 = longitude * (Math.PI / 180.0);
            var d2 = otherLatitude * (Math.PI / 180.0);
            var num2 = otherLongitude * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) + Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }
        [Route("dang-ky-thanh-cong")]
        public ActionResult ThanhCong()
        {
            return View();
        }
        public void SentZaloMess(string oaid, string noidung, string sdt)
        {
            var client = new RestClient("https://openapi.zalo.me/v2.0/oa/message?access_token=" + tokenZalo );
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "{\r\n    \"recipient\": {\r\n        \"user_id\": \""+oaid+"\"\r\n    },\r\n    \"message\": {\r\n        \"text\": \""+noidung+"\"\r\n    }\r\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var reps = JsonConvert.DeserializeObject<Infomation>(response.Content);
            data.LuuLogZalo(oaid, sdt, noidung, reps.error.ToString(), reps.message);
            //Console.WriteLine(response.Content);
        }
        // GET: KhaiBaoDiChuyen/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KhaiBaoDiChuyen/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: KhaiBaoDiChuyen/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: KhaiBaoDiChuyen/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: KhaiBaoDiChuyen/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: KhaiBaoDiChuyen/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
