﻿using CheckIn_CheckOut.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

namespace CheckIn_CheckOut.Controllers
{
    [Authorize]
    public class ThongTinController : ApiController
    {
        DataConnectDataContext data = new DataConnectDataContext();

        public IHttpActionResult Post([FromBody]CheckInOut value)
        {
            try
            {
                string trangthai = "Check-In thành công!";
                string in_out = "In";
                var gethientai = data.Get_ChekcInOut(value.SoDienThoai,value.IDDiemKiemDich).FirstOrDefault();
                if (gethientai != null)
                {
                    if (value.CheckThay == 1)
                    {
                        var res = data.Get_ChekcInOut_Thay(value.SoDienThoai, value.CMND, value.IDDiemKiemDich).FirstOrDefault();
                        if (res is null)
                        {
                            gethientai.TrangThai = "Out";
                        }
                        else
                        {
                            gethientai.TrangThai = res.TrangThai;
                        }
                    }
                    if (gethientai.TrangThai == "Out")
                    {
                        in_out = "In";
                        trangthai = "Check-In thành công!";
                    }
                    else
                    {
                        in_out = "Out";
                        trangthai = "Check-Out thành công!";
                    }
                }
                var ketqua = data.Ins_CheckIn_Out(value.IDDiemKiemDich, value.SoDienThoai, value.ThoiGianCheck, in_out, value.CheckThay, value.HoTen, value.CMND, value.IDXa, value.IDUser1022);
                return Ok(new { error = 200, message = trangthai, data = ketqua });
            }
            catch
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, new { error = -1, message = "Lỗi!" }));
            }
        }
        [HttpGet]
        public IHttpActionResult Get(string sodienthoai, int checkthay, int trang, int soluong)
        {
            var danhsach = data.Get_CaNhan_TheoTrang(sodienthoai, checkthay,trang,soluong).ToList();
            var soluongtong = data.Get_ThongTinCaNhan(sodienthoai, checkthay).Count();
            return Ok(new { error = 200, message = "Danh sách lịch sử!",tongdulieu= soluongtong, data = danhsach });
        }
        [HttpGet]
        public IHttpActionResult DanhSachDonVi(string sodienthoai)
        {
            var danhsach = data.Get_DanhSachDiemKiemDich_SDT(sodienthoai).ToList();
            var soluongtong = danhsach.Count();
            return Ok(new { error = 200, message = "Danh sách đơn vị!", tongdulieu = soluongtong, data = danhsach });
        }
        [HttpGet]
        public IHttpActionResult DanhSachKhach(int donvi, int trang, int soluong)
        {
            var danhsach = data.Get_DanhSach_DiaDiem_TheoTrang(donvi,trang,soluong).ToList();
            var soluongtong = data.Get_Danhsach_TheoDiaDiem(donvi).Count();
            return Ok(new { error = 200, message = "Danh sách khách check-in check-out!", tongdulieu = soluongtong, data = danhsach });
        }
        // GET api/<controller>
        [HttpGet]
        public IHttpActionResult GuiEmail()
        {
            string fromAddress = "guiotptayninh@gmail.com";
            string mailPassword = "123456aA@";       // Mail id password from where mail will be sent.
            string messageBody = "text";

            // Create smtp connection.
            SmtpClient client = new SmtpClient();
            client.Port = 587;//outgoing port for the mail.
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(fromAddress, mailPassword);


            // Fill the mail form.
            var send_mail = new MailMessage();

            send_mail.IsBodyHtml = true;
            //address from where mail will be sent.
            send_mail.From = new MailAddress("guiotptayninh@gmail.com");
            //address to which mail will be sent.           
            send_mail.To.Add(new MailAddress("sondoan1204@gmail.com"));
            //subject of the mail.
            send_mail.Subject = "Test OTP qua email";

            send_mail.Body = messageBody;
            client.Send(send_mail);
            return Ok(new { error = 200, message = "Gửi email thanh cong" });
        }

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public string Post([FromBody]string value)
        //{
        //    return value;
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{

        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}