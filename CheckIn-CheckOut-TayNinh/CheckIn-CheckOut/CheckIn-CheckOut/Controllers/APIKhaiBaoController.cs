﻿using CheckIn_CheckOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CheckIn_CheckOut.Controllers
{
    [Authorize]
    public class APIKhaiBaoController : ApiController
    {
        public class data
        {
            public string sodienthoai { get; set; }
            public string trangthai { get; set; }
            public string latitude { get; set; }
            public string longitude { get; set; }
            public string madiadiem { get; set; }
            public thongtin suckhoe { get; set; }
        }
        public class thongtin
        {
            public int khaiho { get; set; }
            public int madiadiem { get; set; }
            public int cau1 { get; set; }
            public int cau2 { get; set; }
            public int cau3 { get; set; }
            public int cau4 { get; set; }
            public string hoten { get; set; }
            public string sodienthoai { get; set; }
        }
        // GET api/<controller>
        DataConnectDataContext db = new DataConnectDataContext();

        public IHttpActionResult GuiKhaiBao([FromBody]data value)
        {
            try
            {
                if(value.madiadiem==null || value.madiadiem == "")
                {
                    return Ok(new { error = 206, message = "Chưa nhập thông tin địa điểm!" });
                }
                var tt = db.CheckInOuts.Where(n => n.SoDienThoai == value.sodienthoai).OrderByDescending(n=>n.IDRecod).FirstOrDefault();
                if(tt == null)
                {
                    return Ok(new { error = 205, message = "Chưa quan tâm OA Ban chi đạo phòng chóng dịch Covid-19 Tây Ninh!" });
                }
                else if (value.trangthai == "OUT")
                {
                    db.Ins_XacNhanViTri(int.Parse(value.madiadiem), tt.SoDienThoai, DateTime.Now, "OUT", 0, tt.HoTen, null, tt.IDXa, null, tt.OAID, tt.IDTinhDi, null, null, null, tt.DiaChi);
                    return Ok(new { error = 200, message = "Đã rời khỏi Tây Ninh" });
                }
                else if (value.trangthai == "XNTD")
                {
                    if(value.latitude == "" || value.longitude == "" || value.latitude == null || value.longitude == null)
                    {
                        return Ok(new { error = 201, message = "Chưa nhập thông tin vị trí!" });
                    }
                    db.Ins_XacNhanViTri(tt.IDDiemKiemDich, tt.SoDienThoai, DateTime.Now, "XNTD", 0, tt.HoTen, null, tt.IDXa, null, tt.OAID, tt.IDTinhDi, value.latitude + "|" + value.longitude, null, null, tt.DiaChi);
                    return Ok(new { error = 200, message = "Xác nhận thành công!" });
                }else if (value.trangthai == "HANGNGAY")
                {
                    if (value.suckhoe.khaiho == 0)
                    {
                        db.Ins_ToKhaiYTe(tt.HoTen, tt.SoDienThoai, value.suckhoe.cau1, 0, 0, 0, value.suckhoe.madiadiem, value.latitude, value.longitude, value.suckhoe.khaiho, "HANGNGAY");
                        return Ok(new { error = 200, message = "Gửi thông tin thành công!" });
                    }else if(value.suckhoe.khaiho == 1){
                        db.Ins_ToKhaiYTe(value.suckhoe.hoten, value.suckhoe.sodienthoai, value.suckhoe.cau1, 0, 0, 0, value.suckhoe.madiadiem, value.latitude, value.longitude, value.suckhoe.khaiho, "HANGNGAY");
                        return Ok(new { error = 200, message = "Gửi thông tin khai hộ thành công!" });
                    }
                    return Ok(new { error = 202, message = "Chưa chọn thông tin khai hộ!" });
                }
                else
                {
                    return Ok(new { error = 202, message = "Sai mã trạng thái" });
                }
            }
            catch
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, new { error = -1, message = "Lỗi!" }));
            }
        }
       
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}