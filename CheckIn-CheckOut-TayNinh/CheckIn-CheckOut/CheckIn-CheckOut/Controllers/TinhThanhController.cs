﻿using CheckIn_CheckOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CheckIn_CheckOut.Controllers
{
    [Authorize]
    public class TinhThanhController : ApiController
    {
        DataConnectDataContext data = new DataConnectDataContext();
        // GET api/<controller>
        public IEnumerable<GetTinhThanhResult> Get()
        {
            var danhsach = data.GetTinhThanh().ToList();
            return danhsach;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}