﻿using CheckIn_CheckOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CheckIn_CheckOut.Controllers
{
    [Authorize]
    public class QuanHuyenController : ApiController
    {
        DataConnectDataContext data = new DataConnectDataContext();
        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/<controller>/5
        public IEnumerable<GetQuanHuyenResult> Get(int id)
        {
            var danhsach = data.GetQuanHuyen(id).ToList();
            return danhsach;
        }

        // POST api/<controller>
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}