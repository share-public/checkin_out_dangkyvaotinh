﻿using CheckIn_CheckOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CheckIn_CheckOut.Controllers
{
    [Authorize]
    public class XaPhuongController : ApiController
    {
        DataConnectDataContext data = new DataConnectDataContext();
        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/<controller>/5
        public IEnumerable<GetXaPhuongResult> Get(int id)
        {
            var danhsach = data.GetXaPhuong(id).ToList();
            return danhsach;
        }
        //[Route("api/XaPhuong/TraCuuCachLy")]
        [HttpPost]
        public IEnumerable<TraCuuCachLyResult> TraCuuCachLy(int id)
        {
            var res = data.TraCuuCachLy(id);
            return res;
        }
        [HttpGet]
        public IHttpActionResult TraCuuCachLy2(int id)
        {
            var res = data.TraCuuCachLy(id);
            var danhsach = data.ChiTietCachLies.Where(n=>n.IdXa==id).ToList();
            return Ok(new { error = 200, message = "Thông tin!", ketqua = res, danhsachkhuvuc = danhsach });
        }

        // POST api/<controller>
        public string Post2([FromBody] string value)
        {
            return value;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}