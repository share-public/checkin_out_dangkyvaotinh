﻿using CheckIn_CheckOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CheckIn_CheckOut.Controllers
{
    public class DiemKiemDichController : Controller
    {
        DataConnectDataContext data = new DataConnectDataContext();
        // GET: DiemKiemDich
        [Route("diem-kiem-dich")]
        public ActionResult Index()
        {
            if(Session["ThongtinTaiKhoan"] is null || Session["ThongtinTaiKhoan"].ToString() == "")
            {
                return RedirectToAction("Login", "DiemKiemDich");
            }
            int tinh = 0;
            int huyen = 0;
            if(Session["IdTinhThanh"] is null || Session["IdQuanHuyen"] is null)
            {
                ViewData["Huyen"] = new SelectList(data.TinhThanhs.OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh");
                var ds = data.XaPhuongs.Where(n => n.IDQuanHuyen == huyen).ToList();
                ViewData["ListDonVi"] = new SelectList(data.QuanHuyens.OrderBy(c => c.TenQuanHuyen).Where(c => c.IDTinhThanh == tinh), "IDQuanHuyen", "TenQuanHuyen");
                return View(ds);
            }
            else
            {
                tinh = int.Parse(Session["IdTinhThanh"].ToString());
                huyen = int.Parse(Session["IdQuanHuyen"].ToString());
                ViewData["Huyen"] = new SelectList(data.TinhThanhs.OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh",tinh);
                var ds = data.XaPhuongs.Where(n => n.IDQuanHuyen == huyen).ToList();
                ViewData["ListDonVi"] = new SelectList(data.QuanHuyens.OrderBy(c => c.TenQuanHuyen).Where(c => c.IDTinhThanh == tinh), "IDQuanHuyen", "TenQuanHuyen",huyen);
                return View(ds);
            }
        }
        [Route("diem-kiem-dich")]
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            if (Session["ThongtinTaiKhoan"] is null || Session["ThongtinTaiKhoan"].ToString() == "")
            {
                return RedirectToAction("Login", "DiemKiemDich");
            }
            var idTinh = int.Parse(collection["Huyen"]);
            var idhuyen = int.Parse(collection["ListDonVi"]);
            ViewData["Huyen"] = new SelectList(data.TinhThanhs.OrderBy(n => n.Stt), "IDTinhThanh", "TenTinhThanh", idTinh);
            ViewData["ListDonVi"] = new SelectList(data.QuanHuyens.OrderBy(c => c.TenQuanHuyen).Where(c => c.IDTinhThanh == idTinh), "IDQuanHuyen", "TenQuanHuyen", idhuyen);
            var ds = data.XaPhuongs.Where(n => n.IDQuanHuyen == idhuyen).ToList();
            Session["IdTinhThanh"] = idTinh;
            Session["IdQuanHuyen"] = idhuyen;
            return View(ds);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadDSDonVi(string tpid)
        {
            if (string.IsNullOrEmpty(tpid))
                return Json(HttpNotFound());
            var categoryList = GetDonViList(Convert.ToInt32(tpid));
            var categoryData = categoryList.Select(m => new SelectListItem()
            {
                Text = m.TenQuanHuyen,
                Value = m.IDQuanHuyen.ToString()
            });
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }        
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult DoiTrangThai(string tpid, string idxa)
        {
            if (Session["ThongtinTaiKhoan"] is null || Session["ThongtinTaiKhoan"].ToString() == "")
            {
                return Json("Lỗi", JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(tpid))
                return Json(HttpNotFound());
            data.CapNhatTrangThai(int.Parse(idxa), int.Parse(tpid));
            var categoryData = "";
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }
        private IList<QuanHuyen> GetDonViList(int tpid)
        {
            return data.QuanHuyens.OrderBy(c => c.TenQuanHuyen).Where(c => c.IDTinhThanh == tpid).ToList();
        }
        // GET: DiemKiemDich/Details/5
        [Route("thong-tin/{id}")]
        public ActionResult ChiTiet(int id)
        {
            if (Session["ThongtinTaiKhoan"] is null || Session["ThongtinTaiKhoan"].ToString() == "")
            {
                return RedirectToAction("Login", "DiemKiemDich");
            }
            var ds = data.ChiTietCachLies.Where(n => n.IdXa == id).OrderByDescending(n => n.NgayCapNhat).ToList();
            var datachitiet = data.XaPhuongs.FirstOrDefault(n => n.IdXaPhuong == id);
            ViewBag.IDXa = id;
            ViewBag.TenXa = datachitiet.TenXaPhuong;
            ViewBag.TenQuan = datachitiet.QuanHuyen.TenQuanHuyen;
            ViewBag.TEnTinh = datachitiet.QuanHuyen.TinhThanh.TenTinhThanh;
            ViewData["LoaiCachLy"] = new SelectList(data.TinhTrangs.OrderBy(n => n.IDTinhTrang), "IDTinhTrang", "TenTinhTrang", datachitiet.IDTinhTrang);

            return View(ds);
        }

        // GET: DiemKiemDich/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DiemKiemDich/Create
        [HttpPost]
        public ActionResult TaoDiemCachLy(FormCollection collection)
        {
            if (Session["ThongtinTaiKhoan"] is null || Session["ThongtinTaiKhoan"].ToString() == "")
            {
                return RedirectToAction("Login", "DiemKiemDich");
            }
            try
            {
                // TODO: Add insert logic here
                var idxa = int.Parse(collection["idxa"]);
                var nhapdiachia = collection["diadiemcachly"];
                data.Ins_DiemCachLy(idxa, nhapdiachia);
                return RedirectToAction("ChiTiet", "DiemKiemDich", new { id = idxa });
            }
            catch
            {
                return View();
            }
        }

        // GET: DiemKiemDich/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DiemKiemDich/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DiemKiemDich/Delete/5
        public ActionResult Delete(int id, int idxa)
        {
            if (Session["ThongtinTaiKhoan"] is null || Session["ThongtinTaiKhoan"].ToString() == "")
            {
                return RedirectToAction("Login", "DiemKiemDich");
            }
            data.Xoa_diemcachly(id);
            return RedirectToAction("ChiTiet", "DiemKiemDich", new { id = idxa });
        }

        // POST: DiemKiemDich/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [Route("dang-nhap")]
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            //xoa add cookie
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddSeconds(1);
            }
            Session["SoDienThoai"] = null;
            Session["IDDiemKiemDich"] = null;
            Session["TenDiemKiemDich"] = null;
            return RedirectToAction("Login", "DiemKiemDich");
        }
        [Route("dang-nhap")]
        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            var sodienthoai = collection["mobilenumber"];
            var password = collection["password"];
            //Đag làm, gắn cứng tài khoản trước
            //Lấy tài khoản trong db bảng tài khoản sau.
            if(sodienthoai == "taikhoan" && password == "123456")
            {
                Session["ThongtinTaiKhoan"] = "Tai khoản cập nhật thông tin các danh sách điểm cách ly.";
                return RedirectToAction("Index", "DiemKiemDich");
            }
            else
            {
                ViewBag.ThongBao = "Sai thông tin tài khoản! nhập lại";
                return View();
            }

        }
    }
}
